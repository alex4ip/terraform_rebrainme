provider "vscale" {
  token = "${var.vscale_token}"
}

resource "vscale_ssh_key" "zaytsev" {
  name = "Zaytsev key"
  key = "${var.ssh_key_zaytsev}"
}

resource "vscale_scalet" "tf6" {
  count = 2
  name   = "zaytsev_tf6_${count.index}"
  make_from = "centos_7_64_001_master"
  rplan = "medium"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.zaytsev.id}"]
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}
locals {
  ips = ["${vscale_scalet.tf6.*.public_address}"]
}
resource "aws_route53_record" "tf6" {
  count = 2
  zone_id = "${var.aws_zone_id}"
  name    = "il77_${count.index}.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${element(local.ips, count.index)}"]
}
