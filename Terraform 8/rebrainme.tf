provider "vscale" {
  token = "${var.vscale_token}"
}

resource "vscale_ssh_key" "zaytsev" {
  name = "Zaytsev key"
  key = "${var.ssh_key_zaytsev}"
}
resource "random_string" "password" {
  length = 16
  special = true
  override_special = "/@\" "
  min_upper = 4
  min_lower = 6
}

resource "vscale_scalet" "web" {
  name   = "zaytsev_vscale"
  make_from = "centos_7_64_001_master"
  rplan = "small"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.zaytsev.id}"]
  provisioner "remote-exec" {
    inline = [
      "echo -e \"${random_string.password.result}\n${random_string.password.result}\" | passwd root",
    ]
  }  
}

output "ip" {
  value = "${vscale_scalet.web.public_address}"
}
output "password" {
value = "${random_string.password.result}"
}